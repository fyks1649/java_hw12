package vlad;


import vlad.DAO.CollectionFamilyDao;
import vlad.Humans.Family;
import vlad.Humans.Human;
import vlad.Humans.Man;
import vlad.Humans.Woman;
import vlad.DAO.FamilyDao;

public class Main {
    public static void main(String[] args) {
        Human woman = new Woman("Elena", "Lietun");
        Human man = new Man("Dima", "Lietun");

        Family family = new Family(woman, man);

        FamilyDao familyDao = new CollectionFamilyDao();
        familyDao.saveFamily(family);

        System.out.println(familyDao.getAllFamilies());

        System.out.println(familyDao.getFamilyByIndex(0));
        System.out.println(familyDao.getFamilyByIndex(1));

        System.out.println(familyDao.deleteFamily(1));

        System.out.println(familyDao.deleteFamily(family));
    }
}


